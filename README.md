# RainyBot-Plugins
闲来无事，给[RainyBot](https://toscode.gitee.com/baiqitun/RainyBot-Core)写了乱七八糟的插件。
注意，目前插件最高只支持到 RainyBot 2.1.4

### 微博热搜

发送**微博热搜**四个字即可获取实时的微博热搜榜前**10**

* 插件名：WeiboHot
* 关键字：微博热搜
* 版本：1.0
* 最后更新时间：2022-11-1  

效果图如下：

![插件效果图](https://gitee.com/baiqitun/rainy-bot-plugins/raw/master/previews/weibo-hot/preview1.png)

### 百度翻译

可以进行中英文单词互译的插件，发送**百度翻译**四个字后面跟上要翻译的内容即可

* 插件名：BaiduFanyi
* 关键字：百度翻译
* 版本：1.0
* 最后更新时间：2022-6-26  

效果图如下：

![插件效果图](https://godoter.cn/assets/files/2022-06-26/1656207151-211453-fc6cc5ab-89b1-47e4-a01d-80ddd4e581e0.png)

### 缔造者文档

获取反恐精英Online缔造者文档的网址，发送**缔造者文档**五个字后，返回反恐精英Online缔造者文档的网址

* 插件名：CSOLCreatorDoc
* 关键字：缔造者文档
* 版本：1.0
* 最后更新时间：2022-10-31  

效果图如下：

![插件效果图](https://godoter.cn/assets/files/2022-07-12/1657636912-191549-image.png)

### QQ宠物
QQ宠物养成游戏，快领养一只属于你的Q宠宝贝吧
* 插件名：QQPet
* 关键字：q宠、Q宠、Q宠宝贝、q宠宝贝
* 版本：2.1
* 最后更新时间：2022-8-21  
* 命令列表：
  1. 领养，参数：公/母

![插件效果图](https://godoter.cn/assets/files/2022-08-21/1661042028-214888-image.png)

  2. 孵化
  
![插件效果图](https://godoter.cn/assets/files/2022-08-21/1661042084-412754-image.png)

  3. 喂食

![插件效果图](https://godoter.cn/assets/files/2022-08-21/1661044293-208948-image.png)

  4. 清洁

![插件效果图](https://godoter.cn/assets/files/2022-08-21/1661044868-829114-image.png)

  5. 玩耍

![插件效果图](https://godoter.cn/assets/files/2022-08-21/1661044255-53636-image.png)

  6. 数据

![插件效果图](https://godoter.cn/assets/files/2022-08-21/1661044392-502497-image.png)

  7. 背包

![插件效果图](https://godoter.cn/assets/files/2022-08-21/1661044900-647577-image.png)

  8. 签到

![插件效果图](https://godoter.cn/assets/files/2022-08-21/1661044342-607457-image.png)

  9. 帮助

![插件效果图](https://godoter.cn/assets/files/2022-08-21/1661044939-870978-image.png)

### 签到插件增强版

发送**签到**两个字，即可完成签到

* 插件名：PowerSignin
* 关键字：签到
* 版本：1.0.0
* 最后更新时间：2022-9-27

效果图如下：

![插件效果图](https://godoter.cn/assets/files/2022-09-27/1664262618-242801-image.png)

