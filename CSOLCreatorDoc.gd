extends Plugin

func _on_init():
	set_plugin_info("CSOLCreatorDoc","Csol缔造者官方文档", "母鸡啊","1.0","直接输入“缔造者文档”即可")
	
func _on_load():
	register_keyword("缔造者文档", "_get_link",{}, MatchMode.BEGIN)
	register_event([GroupMessageEvent], "trigger_keyword")

func _get_link(keyword, parsed, arg, event):
	event.reply("https://tw.beanfun.com/cso/STUDIO/api/index.html", true, true)
