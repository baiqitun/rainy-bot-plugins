# 微博热搜插件
extends Plugin

func _on_init():
	set_plugin_info("BaiduFanyi","百度翻译", "母鸡啊","1.0","直接输入“百度翻译”，后面跟上要翻译的内容即可")
	
func _on_load():
	register_keyword("百度翻译", "_do_search",{}, MatchMode.BEGIN)
	register_event([GroupMessageEvent], "trigger_keyword")
	
func _do_search(keyword, parsed, arg, event):
	if arg != null || !arg.empty():
		# 手动设置Headers	
		var headers = [
			'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36 Edg/101.0.1210.47',
			"Content-Type: application/json"
		]
		print(arg)
		var data = {
			"kw": arg.trim_prefix(" ")
		}
		var json = JSON.new()
		print(json.stringify(data))

		var _result:HttpRequestResult = await Utils.send_http_post_request("https://fanyi.baidu.com/sug", json.stringify(data), PackedStringArray(headers))

		var dict = _result.get_as_dic()
		if dict["errno"] != 0:
			event.reply(dict["errmsg"], true, true)
		else:
			if len(dict['data']) > 0:
				var msg = ''
				for ele in dict['data']:
					msg += ele['k'] + ": "
					msg += ele['v']
					msg += '\n'
				event.reply(msg)
	pass
	
	
	
