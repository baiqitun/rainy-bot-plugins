#RainyBot插件模板
#相关帮助文档，示例插件及API请访问 https://docs.rainybot.dev 进行查阅
extends Plugin #默认继承插件类，请勿随意改动


#可以在此处定义各种插件范围的全局变量/常量/枚举等，例如：
#var data:Dictionary = {}
#var lib:Plugin = null
#var connected:bool = true
const user_data_template = {
	"keep": 0,
	"nickname": "",
	"last_signin": null
}

const luck_flags = [
		{
			"full": "★",
			"half": "☆"
		},
		{
			"full": "♠",
			"half": "♤"
		},
		{
			"full": "♥",
			"half": "♡"
		},
		{
			"full": "♣",
			"half": "♧"
		},
		{
			"full": "♫",
			"half": "♪"
		},
	]

var content_path = null
var signin_scene = null
var bg_images = []
var bg_textures = []

#将在此插件的文件被读取时执行的操作
#必须在此处使用set_plugin_info函数来设置插件信息，插件才能被正常加载
#例如：set_plugin_info("example","示例插件","author","1.0","这是插件的介绍")
#可以在此处初始化和使用一些基本变量，但不建议执行其它代码，可能会导致出现未知问题
func _on_init()->void:
	set_plugin_info("powersignin","签到增强版插件","母鸡啊","1.0.0","一个签到插件")
	content_path = get_plugin_path() + "PowerSignin"
	pass
	

#将在RainyBot与协议后端恢复连接时执行的操作
#可以在此处进行一些与连接状态相关的操作，例如恢复连接后发送通知等
func _on_connect()->void:
	#connected = true
	pass


#将在此插件被完全加载后执行的操作
#可以在此处进行各类事件/关键词/命令的注册，以及配置/数据文件的初始化等
func _on_load()->void:
	register_event([GroupMessageEvent,FriendMessageEvent],"trigger_keyword")
	register_keyword("签到",do_signin,{},MatchMode.BEGIN)
	#register_console_command("","")
	#init_plugin_config({})
	# 初始化数据文件
	init_plugin_data()
	pass


#将在所有插件被完全加载后执行的操作
#可以在此处进行一些与其他插件交互相关的操作，例如获取某插件的实例等
#注意：如果此插件硬性依赖某插件，推荐在插件信息中注册所依赖的插件，以确保其在此插件之前被正确加载
func _on_ready()->void:
	var image_path = content_path + "/image/1024×600"
	bg_images = suffix_filter(image_path, [".jpg", ".png"])
	
	for bg_image in bg_images:
		bg_textures.append(load(bg_image))
	
	Console.print_success("纹理数量：" + str(len(bg_textures)))
	signin_scene = await load_scene(content_path + "/power_signin.tscn", true)


#将在此插件运行的每一秒执行的操作
#可在此处进行一些计时，或时间判定相关的操作，例如整点报时等
func _on_process()->void:
	#var _runtime:int = get_plugin_runtime()
	pass


#将在RainyBot与协议后端断开连接时执行的操作
#可以在此处进行一些与连接状态相关的操作，例如断开连接后暂停某些任务的运行等
func _on_disconnect()->void:
	#connected = false
	pass


#将在此插件即将被卸载时执行的操作
#可在此处执行一些自定义保存或清理相关的操作，例如储存自定义的文件或清除缓存等
#无需在此处取消注册事件/关键词/命令，或者对内置的配置/数据功能进行保存，插件卸载时将会自动进行处理
func _on_unload()->void:
	#lib.save_file("")
	pass

# 对目录内的文件进行过滤，只保留指定后缀的文件
func suffix_filter(path: String, suffix_list: Array):
	var files = []
	Console.print_text("开始扫描路径")
	Console.print_text(path)
	var dir = Directory.new()
	var result = dir.open(path)
	if result == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				pass
			else:
				for suffix in suffix_list:
					if file_name.ends_with(suffix):
						files.append(path+"/"+file_name)
						
					
			file_name = dir.get_next()
	else:
		Console.print_error("尝试访问路径时出错。")
		Console.print_error("错误码："+str(result))
	return files


func luck_words(max_random: int):
	var words: Array = ["逛街", "摸鱼", "晒太阳", "和小伙伴开黑", "吃零食",  "发呆", "在床上挺尸", "修仙", "追番", "写代码", "emo", "斗图", "过生日"]	
	randomize()
	var good_words_num = randi() % max_random
	print("宜:" + str(good_words_num))
	var bad_words_num = randi() % max_random
	print("忌:" + str(bad_words_num))
	var good_words = []
	for i in range(0, good_words_num + 1):
		randomize()
		var good_words_idx = randi() % len(words)
		good_words.append(words[good_words_idx])
		words.remove_at(good_words_idx)
	
	var bad_words = []
	for i in range(0, bad_words_num + 1):
		randomize()
		var bad_words_idx = randi() % len(words)
		bad_words.append(words[bad_words_idx])
		words.remove_at(bad_words_idx)
	return {"good": good_words, "bad": bad_words}

func random_luck(max_random):
	randomize()
	var random_flag_idx = randi() % len(luck_flags)
	var flag = luck_flags[random_flag_idx]
	randomize()
	var random_value = randi() % max_random + 1
	var luck_text = ""
	print(random_value)
	if random_value % 2 == 1:
		luck_text = flag["half"]
		random_value -= 1
		
	for i in range(0, random_value/ 2):
		luck_text = flag["full"] + luck_text
	
	return luck_text

func do_signin(keyword,parsed,arg,event):
	Console.print_success("执行")
	var owner_id = event.get_sender_id()
	
	
	var user_data = get_plugin_data(owner_id)
	if user_data == null:
		user_data = user_data_template.duplicate()
		set_plugin_data(owner_id, user_data)
	
	
	
	var now_time = Time.get_time_dict_from_system()
	var today = Time.get_date_dict_from_system()
	
	
	if today == user_data["last_signin"]:
		event.reply("今天你已经签到过了哦~")
		return
	
	var user_name = event.get_sender().get_name()
	user_data["last_signin"] = today
	set_plugin_data(owner_id, user_data)
	
	# TODO连续签到功能
	
	var model = {}
	randomize()
	var texture_idx = randi_range(0, len(bg_textures) - 1)
	model["texture"] = bg_textures[texture_idx]
	model["timetip_right"] = "{year}年{month}月{day}日".format(today)
	
	# todo 问候语时间bug
	if now_time["hour"] <= 9:
		model["greetings_left"] = "早上好呀！"
	elif now_time["hour"] <= 11:
		model["greetings_left"] = "上午好呀！"
	elif now_time["hour"] <= 13:
		model["greetings_left"] = "中午好呀！"
	elif now_time["hour"] <= 20:
		model["greetings_left"] = "下午好呀！"
	else:
		model["greetings_left"] = "晚上好啊！"
	model["greetings_right"] = user_name
	model["luck"] = random_luck(10)
	
	var luck_words_pair = await luck_words(3)
	model["good"] = "，".join(luck_words_pair["good"])
	model["bad"] = "，".join(luck_words_pair["bad"])
	

	
	Console.print_text(model)
	signin_scene.render(model)
	
	var image = await get_scene_image(signin_scene, Vector2(1024,600))
	var image_message:ImageMessage = ImageMessage.init(image)
	event.reply("萌萌酱正在画出来~")
	event.reply(image_message)
