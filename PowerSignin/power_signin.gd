extends Control


func _ready():
	pass 


func _process(delta):
	pass
	
func render(model):
	$Background.global_position.x = 512
	$Background.global_position.y = 300
	
	$Background.texture = null
	$Background.texture = await model["texture"]
	if model["greetings_right"].length() > 8:
		model["greetings_right"] = model["greetings_right"].substr(0, 7) + "..."
	$CenterContainer/VBoxContainer/DateTip/DateTipRight.text = model["timetip_right"]
	$CenterContainer/VBoxContainer/Greetings/GreetingsLeft.text = model["greetings_left"]
	$CenterContainer/VBoxContainer/Greetings/GreetingsRight.text = model["greetings_right"]
	$CenterContainer/VBoxContainer/Info/Luck.text = model["luck"]
	$CenterContainer/VBoxContainer/Info/Good.text = model["good"]
	$CenterContainer/VBoxContainer/Info/Bad.text = model["bad"]
