# 微博热搜插件
extends Plugin

func _on_init():
	set_plugin_info("WeiboHot","微博热搜", "母鸡啊","1.0","微博热搜，直接输入“微博热搜”,四个字就行")
	
func _on_load():
	register_keyword("微博热搜", "_do_search",{}, MatchMode.EQUAL)
	register_event([GroupMessageEvent], "trigger_keyword")
	
func _do_search(keyword, parsed, arg, event):
	# 手动设置Headers	
	var headers = [
			'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36 Edg/101.0.1210.47',
		]
	var _result:HttpRequestResult = await Utils.send_http_get_request("https://weibo.com/ajax/side/hotSearch", PackedStringArray(headers))
	var dict = _result.get_as_dic()
	# 热门
	var hot = dict["data"]["hotgov"]
	# 实时榜单
	var realtime_list = dict["data"]["realtime"]
	var msg = "\n"
	if ! hot.is_empty():
		msg += "🔥" + hot["name"] + hot["url"] + "\n"
	
	for index in range(0, 5):
		var emoji = ""
		# 奖杯
		if index < 10:
			
			# 奖杯
			if index == 0:
				emoji = "🏆"
			# 银牌
			elif index == 1:
				emoji = "🥈"
			# 铜牌
			elif index == 2:
				emoji = "🥉"
			# 4
			elif index == 3:
				emoji = "4️⃣"
			# 5
			elif index == 4:
				emoji = "5️⃣"
			# 6
			elif index == 5:
				emoji = "6️⃣"
			# 7
			elif index == 6:
				emoji = "7️⃣"
			# 8
			elif index == 7:
				emoji = "8️⃣"
			# 9
			elif index == 8:
				emoji = "9️⃣"
			# 10
			elif index == 9:
				emoji = "🔟"
			else:
				emoji = "+1)"
			var word: String =  dict["data"]["realtime"][index]["word"]
			var word_scheme: String = "#" + word + "#"
			var url: String = "https://s.weibo.com/weibo?q=" + word_scheme.uri_encode() + "&topic_ad="
			msg += emoji + word + url +"\n"

		else:
			break;
		
	event.reply(msg, true, true)

	msg = "\n"
	
	for index in range(5, len(realtime_list)):
		var emoji = ""
		# 奖杯
		if index < 10:
			
			# 奖杯
			if index == 0:
				emoji = "🏆"
			# 银牌
			elif index == 1:
				emoji = "🥈"
			# 铜牌
			elif index == 2:
				emoji = "🥉"
			# 4
			elif index == 3:
				emoji = "4️⃣"
			# 5
			elif index == 4:
				emoji = "5️⃣"
			# 6
			elif index == 5:
				emoji = "6️⃣"
			# 7
			elif index == 6:
				emoji = "7️⃣"
			# 8
			elif index == 7:
				emoji = "8️⃣"
			# 9
			elif index == 8:
				emoji = "9️⃣"
			# 10
			elif index == 9:
				emoji = "🔟"
			else:
				emoji = "+1)"
			var word: String =  dict["data"]["realtime"][index]["word"]
			var word_scheme: String = "#" + word + "#"
			var url: String = "https://s.weibo.com/weibo?q=" + word_scheme.uri_encode() + "&topic_ad="
			msg += emoji + word + url +"\n"

		else:
			break;
		
	event.reply(msg, true, true)