#QQ宠物插件 最后更新时间：2022年8月21日
extends Plugin
#插件常量
const PLUGIN_ID = "QQPet"
const PLUGIN_NAME = "QQ宠物"
const PLUGIN_VERSION = "2.1"
const PLUGIN_AUDITOR = "母鸡啊"
const PLUGIN_KEYWORDS = ["Q宠", "q宠", "Q宠宝贝", "q宠宝贝"]

func _on_init()->void:
	set_plugin_info(PLUGIN_ID, PLUGIN_NAME, PLUGIN_AUDITOR, PLUGIN_VERSION,"无")
	

#将在此插件被完全加载后执行的操作
#可以在此处进行各类事件/关键词/命令的注册，以及配置/数据文件的初始化等
func _on_load()->void:
	# 注册所有关键字
	for keyword in PLUGIN_KEYWORDS:
		register_keyword(keyword, command_exec, {}, MatchMode.BEGIN)
	register_event([GroupMessageEvent,FriendMessageEvent],"trigger_keyword")
	# 初始化数据文件
	init_plugin_data()
	pass
	
func command_exec(keyword, parsed, line: String, event):
	Console.print_text(keyword)
	Console.print_text(parsed)
	
	var args = line.split(" ", false)
	var first_arg = args[0]
	var owner_id = event.get_sender_id()
	Console.print_text(args)
	match first_arg:
		"领养":
			if len(args) < 2:
				assistant_reply_message(event, "领养",  "PET_SEX_UNKNOWN")
				return
				
			var sex_name = args[1]
			if sex_name != PetConstant.SEX_MAPPING[true] and sex_name !=  PetConstant.SEX_MAPPING[false]:
				assistant_reply_message(event, "领养",  "UNKNOWN_PARAMETER")
				return
			
			if pet_existed(owner_id) and not pet_died(owner_id):
				assistant_reply_message(event, "领养",  "PET_EXISTED")
				return

			pet_adopt(event, "Q宠宝贝",   PetConstant.SEX_MAPPING[sex_name])
			return
		"孵化":
			if not pet_existed(owner_id):
				reply_common(event, "PET_NOTEXISTED")
				return
				
			if pet_died(owner_id):
				reply_common(event, "PET_DIED")
				return
				
			var stage_value = get_pet_stage_value(owner_id)
			if stage_value >  PetConstant.STAGE_MAPPING["待孵化"]:
				assistant_reply_message(event, "孵化", "PET_HITCHED")
				return
			
			pet_hitch(event)
			return
		"喂食":
			if not check_passed(event):
				Console.print_text("基础验证失败")
				return
			
			if pet_full(owner_id):
				var user_data = get_plugin_data(owner_id)
				pet_reply_message_with_param(event, "喂食",  "PET_FULL", [user_data["pet"]["call"]])
				return
			
			if not bag_empty(owner_id):
				Console.print_text("背包为空")
				assistant_reply_message(event, "喂食",  "BAG_EMPTY")
				return
			
			# 使用物品
#			var food_id = convert(args[1], TYPE_INT)
			use_goods(event, "喂食", "hungry", null)
			return
		"清洁":
			if not check_passed(event):
				Console.print_text("基础验证失败")
				return
			
			if pet_very_clean(owner_id):
				var user_data = get_plugin_data(owner_id)
				pet_reply_message_with_param(event, "清洁",  "PET_VERY_CLEAN", [user_data["pet"]["call"]])
				return
			
			use_goods(event, "清洁", "clean", null)
			return
		"玩耍":
			if not check_passed(event):
				Console.print_text("基础验证失败")
				return
			
			pet_play(event)
			return
		"背包":
			if not check_passed(event):
				return
			
			pet_bag_info(event)
			return 
		"数据":
			if not check_passed(event):
				return
			pet_info(event)
			return
		"签到":
			if not check_passed(event):
				return
			
			pet_signin(event)
			return
		"帮助":
			help(event)
			return
			
# 基础检查 通过返回true,没通过返回false
func check_passed(event) -> bool:
	var owner_id = event.get_sender_id()
	# 是否已领养宠物
	if not pet_existed(owner_id):
		reply_common(event, "PET_NOTEXISTED")
		return false
	# 宠物是否已死亡
	if pet_died(owner_id):
		reply_common(event, "PET_DIED")
		return false
	# 宠物是否已孵化
	var stage_value = get_pet_stage_value(owner_id)
	if stage_value <  PetConstant.STAGE_MAPPING["宝宝"]:
		reply_common(event, "PET_UNBORN")
		return false
	
	# 通过基础检查
	return true
	
func pet_existed(owner_id):
	if get_plugin_data(owner_id) == null:
		return false
	return true

# 宠物是否已经吃饱了
func pet_full(owner_id):
	var user_data = get_plugin_data(owner_id)
	if user_data["pet"]["hungry"] < PetConstant.MAX:
		return false
	
	return true
	
func pet_very_clean(owner_id):
	var user_data = get_plugin_data(owner_id)
	if user_data["pet"]["clean"] < PetConstant.MAX:
		return false
	
	return true
	
func pet_very_happy(owner_id):
	var user_data = get_plugin_data(owner_id)
	if user_data["pet"]["happy"] < PetConstant.MAX:
		return false
	
	return true

# 背包是否有物品
func bag_empty(owner_id):
	var bag_data = get_bag_data(owner_id)
	if len(bag_data) == 0:
		return false
		
	return true

# 背包里是否有指定物品
func bag_has_goods(owner_id, type, food_id):
	var bag_data = get_bag_data(owner_id)
	if len(bag_data) == 0:
		return false
	if bag_data[type][food_id]  == null:
		return false
	if bag_data[type][food_id] == 0:
		return false
	return true	

# 领养宠物
func pet_adopt(event, pet_name: String, pet_sex):
	var owner_id = event.get_sender_id()
	var new_user_data =  PetConstant.DEFAULT_USER_DATA.duplicate()
	new_user_data["owner_id"] = owner_id
	new_user_data["pet"]["name"] = pet_name
	new_user_data["pet"]["sex"] = pet_sex
	set_plugin_data(owner_id, new_user_data)
	assistant_reply_message(event, "领养",  ReplyQuerier.SUCCESS_KEY)
	var adopt_path_template = "QQPet/action/{sex_name}/Adopt.gif"
	assistant_reply_image(event, adopt_path_template.format({"sex_name": PetConstant.SEX_MAPPING[pet_sex]}))
	#  提示获得初始物品奖励信息
	
	var bag_data = new_user_data["bag"]
	var bag_info_msg = "\n🎉恭喜你获得🎉：\n"
	var bag_empty = true
	Console.print_text(bag_data)
	for goods_type in bag_data:
		
		bag_empty = false
		var type_empty = true
		if len(bag_data[goods_type]) == 0:
			continue;
		
		for goods_id in bag_data[goods_type]:
			var goods_num = bag_data[goods_type][goods_id]
			if goods_num == null or goods_num <= 0:
				continue;
			
			var goods = GoodsQuerier.query(goods_type, goods_id)
			if goods == null:
				continue
			
			bag_info_msg += ("🎁"+str(goods["name"])+" * " + str(goods_num) + "\n")

	if bag_empty:
		return
		
	event.reply(bag_info_msg, true, true)
	return

# 孵化宠物
func pet_hitch(event):
	var owner_id = event.get_sender_id()
	var user_data = get_plugin_data(owner_id)
	var pet_data = user_data["pet"]
	Console.print_text(pet_data)
	
	var action_path = ActionQuerier.query(pet_data["sex"], pet_data["stage"], "孵化")
	if action_path == null:
		reply_common(event, "PET_DISABLE")
		return
		
	# 修改宠物的成长阶段
	user_data["pet"]["stage"] =  PetConstant.STAGE_MAPPING["宝宝"]
	set_plugin_data(owner_id, user_data)
	
	# 返回孵化动画
	var image_path = get_plugin_path() + action_path
	var image_message = ImageMessage.init_path(image_path)
	assistant_reply_message(event, "孵化",  ReplyQuerier.SUCCESS_KEY)
	event.reply(image_message, true, true)

#玩耍
func pet_play(event):
	var owner_id = event.get_sender_id()
	var user_data = get_plugin_data(owner_id)
	
	var action_path = ActionQuerier.query(user_data["pet"]["sex"], user_data["pet"]["stage"], "玩耍")
	if action_path == null:
		reply_common(event, "PET_DISABLE")
		return
	
	if user_data["pet"]["hungry"] <= 100:
		pet_reply_message_with_param(event, "玩耍", "PET_VERY_HUNGRY", [ user_data["pet"]["call"]])
		return
	
	if user_data["pet"]["clean"] <= 100:
		pet_reply_message_with_param(event, "玩耍", "PET_VERY_DIRTY", [ user_data["pet"]["call"]])
		return
	
	Console.print_text("增加心情值")
	#增加心情	值
	var random_add_happy = randi_range(100, 800)
	var happy_tmp = PetConstant.MAX - user_data["pet"]["happy"]
	var happy_change = 0
	if random_add_happy >= happy_tmp:
		happy_change = happy_tmp
		user_data["pet"]["happy"] = PetConstant.MAX
	else:
		happy_change = random_add_happy
		user_data["pet"]["happy"] = user_data["pet"]["happy"] + random_add_happy
	
	Console.print_text("降低饱食度")
	#降低饱食度
	var random_sub_hungry = randi_range(100, 400)
	var hungry_change = 0
	if random_sub_hungry < user_data["pet"]["hungry"]:
		hungry_change = random_sub_hungry
		user_data["pet"]["hungry"] -= random_sub_hungry	
	else:
		hungry_change = user_data["pet"]["hungry"]
		user_data["pet"]["hungry"] = 0
	
	Console.print_text("降低清洁度")
	#降低清洁度
	var random_sub_clean = randi_range(100, 400)
	var clean_change = 0
	if random_sub_clean < user_data["pet"]["clean"]:
		clean_change = random_sub_clean
		user_data["pet"]["clean"] -= random_sub_clean
	else:
		clean_change = user_data["pet"]["clean"]
		user_data["pet"]["clean"] = 0
		
	pet_reply_message_with_param(event, "玩耍",  ReplyQuerier.SUCCESS_KEY, [user_data["pet"]["call"]])
	assistant_reply_image(event, action_path)
	assistant_reply_message_with_param(event, "玩耍",  ReplyQuerier.SUCCESS_KEY, [happy_change, hungry_change, clean_change])	
	
# 查看背包
func pet_bag_info(event):
	var owner_id = event.get_sender_id()
	var bag_info_msg = ""
	var bag_data = get_bag_data(owner_id)
		
	var bag_empty = true
	for goods_type in bag_data:
		var temp_info_msg = "\n"
		if goods_type == "hungry":
			temp_info_msg += "🍗食品列表：\n"
		elif goods_type == "clean":
			temp_info_msg += "🚿清洁用品列表：\n"
		
		var type_empty = true
		for goods_id in bag_data[goods_type]:
			var goods_num = bag_data[goods_type][goods_id]
			if goods_num == null:
				continue;
			
			var goods = GoodsQuerier.query(goods_type, goods_id)
			if goods == null:
				continue
			
			type_empty = false
			bag_empty = false
			temp_info_msg += ( "id:" + str(goods_id) + ", " +str(goods["name"])+" * " + str(goods_num) + "\n")
		if not type_empty:
			bag_info_msg += temp_info_msg
	
	if bag_empty:
		event.reply(ReplyQuerier.COMMON_DATA["BAG_EMPTY"], true, true)
		return

	event.reply(bag_info_msg, true, true)
	return

# 签到
func pet_signin(event):
	
	var owner_id = event.get_sender_id()
	var user_data = get_plugin_data(owner_id)
	if user_data["signin"]["last_signin"] == Time.get_date_dict_from_system():
		assistant_reply_message(event, "签到",  "SIGNIN_EXISTED")
		return
		
	var msg = "\n🎉恭喜你签到成功，获得了以下签到奖励🎉：\n"
	
	var food_size = len(GoodsQuerier.GOODS_DATA["hungry"])
	var rand_food_id = randi_range(0, food_size - 1)
	var rand_food_num = randi_range(1, 5)
	var food_name = GoodsQuerier.query_name("hungry", rand_food_id)
	give_goods(owner_id, "hungry", rand_food_id, rand_food_num)
	msg += "🍗" + food_name + "*" + str(rand_food_num) + "\n"
	
	var clean_size = len(GoodsQuerier.GOODS_DATA["clean"])
	var rand_clean_id = randi_range(0, clean_size - 1)
	var rand_clean_num = randi_range(1, 5)
	var clean_name = GoodsQuerier.query_name("clean", rand_clean_id)
	give_goods(owner_id, "clean", rand_clean_id, rand_clean_num)
	msg += "🚿" + clean_name + "*" + str(rand_clean_num) + "\n"
	
	# 更新用户数据
	user_data["signin"]["last_signin"] = Time.get_date_dict_from_system()
	set_plugin_data(owner_id, user_data)
	event.reply(msg, true, true)

# 帮助
func help(event):
	var msg = "\n"
	msg += "📘关键字列表：\n"
	msg += "  ● Q宠\n"
	msg += "  ● q宠\n"
	msg += "  ● Q宠宝贝\n"
	msg += "  ● q宠宝贝\n"
	msg += "************************\n"
	msg += "📘命令列表：\n"
	msg += "  ● 领养，参数：公/母\n"
	msg += "  ● 孵化\n"
	msg += "  ● 喂食\n"
	msg += "  ● 清洁\n"
	msg += "  ● 玩耍\n"
	msg += "  ● 数据\n"
	msg += "  ● 背包\n"
	msg += "  ● 签到\n"
	msg += "  ● 帮助\n"
	event.reply(msg, true, true)

const PET_INFO: String = "
⭐性一别⭐：\t{0}
⭐阶一段⭐：\t{1}
⭐元一宝⭐：\t{2}
⭐清洁度⭐：\t{3}
⭐饱食度⭐：\t{4}
⭐心情值⭐：\t{5}"

#⭐成长值：\t{1}\n

func pet_info(event):
	var pet_data = get_pet_data(event.get_sender_id())
	var sex_flag = PetConstant.SEX_MAPPING[pet_data["sex"]]
	var stage_name = PetConstant.STAGE_MAPPING[pet_data["stage"]]
	var info_msg = PET_INFO.format([sex_flag,stage_name, pet_data["money"], pet_data["clean"], pet_data["hungry"], pet_data["happy"]])
	event.reply(info_msg, true, true)
	
func sex_name2value(sex_name: String):
	return true if sex_name ==  PetConstant.SEX_MAPPING[true] else false
	
func sex_value2name(sex_value: bool):
	return  PetConstant.SEX_MAPPING[true] if sex_value else  PetConstant.SEX_MAPPING[false]


# 获取宠物数据
func get_pet_data(owner_id) :
	return get_plugin_data(owner_id)["pet"]

# 获取宠物成长阶段名称
func get_pet_stage_name(owner_id):
	var stage_value = get_plugin_data(owner_id)["stage"]
	return  PetConstant.SEX_MAPPING[stage_value]
	
# 获取宠物成长阶段值
func get_pet_stage_value(owner_id):
	return get_pet_data(owner_id)["stage"]

# 获取背包物品数据
func get_bag_data(owner_id) -> Dictionary:
	return get_plugin_data(owner_id)["bag"]

func pet_died(owner_id):
	var pet_data = get_pet_data(owner_id)
	Console.print_text(pet_data)
	if pet_data["stage"] ==  PetConstant.STAGE_MAPPING["已死亡"]:
		return true
	return false

func give_goods(owner_id, type, id, num):
	var user_data = get_plugin_data(owner_id)
	var goods_num = user_data["bag"][type][id]
	if goods_num == null or goods_num == 0:
		Console.print_text("物品覆盖")
		user_data["bag"][type][id] = num
		set_plugin_data(owner_id, user_data)
		return
	else:
		user_data["bag"][type][id] = goods_num + num
		set_plugin_data(owner_id, user_data)
		return
	

# 使用消耗性物品
func use_goods(event, order, type, goods_id):
	var owner_id = event.get_sender_id()
	var user_data = get_plugin_data(owner_id)
	if goods_id != null:
		# TODO 实现使用指定物品
		pass
	else:
		if len(user_data["bag"][type]) == 0:
			pet_reply_message_with_param(event, order, "BAG_EMPTY", [user_data["pet"]["call"]])
			return
			
		for id in user_data["bag"][type]:
			var goods_num = user_data["bag"][type][id]
			Console.print_text("物品个数：")
			Console.print_text( str(goods_num))
			if goods_num == null or goods_num <= 0:
				user_data["bag"][type].erase(id)
				continue
				
			var goods = GoodsQuerier.query(type, id)
			if goods == null:
				user_data["bag"][type].erase(id)
				continue
			
			var action_path = ActionQuerier.query(user_data["pet"]["sex"], user_data["pet"]["stage"], order)
			if action_path == null:
				reply_common(event, "PET_DISABLE")
				return
			
			Console.print_text("开始使用物品")
			# 使用物品
			user_data["bag"][type][id] -= 1
			if user_data["bag"][type][id] <= 0:
				user_data["bag"][type].erase(id)
				
			var tmp = PetConstant.MAX - user_data["pet"][type]
			Console.print_text("物品使用完毕")
			
			if goods["value"] >= tmp:
				user_data["pet"][type] = PetConstant.MAX
				set_plugin_data(owner_id, user_data)
				pet_reply_message_with_param(event, order, ReplyQuerier.SUCCESS_KEY, [user_data["pet"]["call"]])
				assistant_reply_image(event, action_path)
				assistant_reply_message_with_param(event, order, ReplyQuerier.SUCCESS_KEY, [goods["name"], tmp])
				return
				
			else:
				user_data["pet"][type] = user_data["pet"][type] + goods["value"]
				set_plugin_data(owner_id, user_data)
				pet_reply_message_with_param(event, order, ReplyQuerier.SUCCESS_KEY, [user_data["pet"]["call"]])
				assistant_reply_image(event, action_path)
				assistant_reply_message_with_param(event, order, ReplyQuerier.SUCCESS_KEY, [goods["name"], goods["value"]])
				return
			
# 计算每一级的经验值上限
func get_max_exp_by_level(level):
	return level * 200

func pet_reply_message(event, order, key):
	event.reply(ReplyQuerier.query_pet_reply(order, key), true, true)

# 宠物向用户回复的消息
func pet_reply_message_with_param(event, order, key, params):
	if params != null and len(params) > 0:
		event.reply(ReplyQuerier.query_pet_reply_with_params(order, key, params), true, true)
		return

# 小助手向用户回复的消息
func assistant_reply_message(event, order, key):
	event.reply(ReplyQuerier.query_assistant_reply(order, key), true, true)


func assistant_reply_message_with_param(event, order, key, params):
	if params != null and len(params) > 0:
		event.reply(ReplyQuerier.query_assistant_reply_with_params(order, key, params), true, true)
		return
	
func reply_common(event, key):
	event.reply(ReplyQuerier.COMMON_DATA[key], true, true)
	
# 宠物小助手向用户回复图片
# * event
# * path  "QQPet/action/male/Adopt.gif"
func assistant_reply_image(event, path):
	var image_path = get_plugin_path() + path
	Console.print_text(image_path)
	var image_message = ImageMessage.init_path(image_path)
	event.reply(image_message, true, true)

class ReplyQuerier:
	const SUCCESS_KEY = "SUCCESS"
	
	const COMMON_DATA = {
		"PET_NOTEXISTED": [
				"你还没有领养过Q宠宝贝，请使用【领养 】命令领养一只Q宠宝贝吧!"
		],
		"PET_UNBORN": [
				"你的Q宠宝贝还没有孵化，请先使用【孵化】命令进行孵化！"
		],
		"PET_DIED": [
				"你的Q宠宝贝已经死亡了，如果想要一只新的Q宠宝贝请使用【领养】命令领养一只新的Q宠宝贝!"
		],
		"UNKNOWN_PARAMETER": [
				"无法识别的参数!"
		],
		"NULL_PARAMETER": [
				"参数不能为空，"
		],
		"PET_DISABLE": [
				"你的Q宠宝贝还不能做这件事哦!"
		],
		"BAG_EMPTY": [
				"你的背包现在空空如也~"
		],
		"GOODS_NOT_FOUND": [
			"背包里没有找到此物品"
		]
	}
	
	const ASSISTANT_DATA = {
		"领养": {
			SUCCESS_KEY: [
				"恭喜你成功领养了一只Q宠宝贝，要好好爱护它哦！"
			],
			"PET_EXISTED": [
				"你已经领养过一只Q宠宝贝了，要好好爱护它哦!"
			],
			"PET_SEX_UNKNOWN": [
				"你还没有指定宠物的性别参数哦！参数：公/母，例如：q宠 领养 公"
			],
		},
		"孵化": {
			SUCCESS_KEY: [
				"恭喜，你的宠物孵化成功了！"	
			],
			"PET_HITCHED": [
				"你的宠物已经孵化了，不需要再进行孵化了"
			]
		},
		"喂食": {
			SUCCESS_KEY: [
				"恭喜你成功了使用了{0},  饱食度 + {1}"
			],
			"UNKNOWN_PARAMETER": [
				"无法识别的参数，想要喂养宠物， 请先使用【查看背包】命令查看背包中可以使用的物品"
			],
			"NULL_PARAMETER": [
				"食物id不能为空，想要喂养宠物， 请先使用【查看背包】命令查看背包中的物品的id"
			]
		},
		"清洁": {
			SUCCESS_KEY: [
				"恭喜你成功了使用了{0},  清洁度 + {1}"
			]
		},
		"玩耍": {
			SUCCESS_KEY: [
				"心情值 + {0}，饱食度 - {1}，清洁度 - {2}"
			]
		},
		"数据": {
			SUCCESS_KEY: [
"\n
⭐性一别⭐：\t{0}\n
⭐阶一段⭐：\t{1}\n
⭐元一宝⭐：\t{2}\n
⭐清洁度⭐：\t{3}\n
⭐饱食度⭐：\t{4}\n
⭐心情值⭐：\t{5}\n"
				]
			},
		"签到":  {
			"SIGNIN_EXISTED": [
				"你今天已经签到过了~"
			]
		}
	}
	
	const PET_DATA = {
		"孵化": {
			"PET_DISABLE": [
				"{0}，不要为难我好不好~😔!"
			],
		},
		"喂食": {
			SUCCESS_KEY: [
				"哎呦，我弯不下腰了，好撑啦，待会肯定是没法和朋友赛跑了。😋",
				"吃那么多，我在想，从哪里开始减肥了……",
				"肚子饱饱，心情好好！{0}，你对我越来越好啦~",
				"再怎么不能亏待了自己的肚子，{0}，你说是吧？",
				"当然得先吃饱，才有力气去减肥呀！",
				"最重要的是吃饱，现在暂别减肥，哈哈……",
				"{0}，人是铁饭是钢，一顿不吃浑身没力气！😋"
			],
			"LEVEL_UP": [
				"{0}，我又升级啦！是不是变得更可爱了呢？",
				"真棒，我又升级了，都是{0}无微不至的照顾我才长得这么快的呢~",
				"{0}，我又升了一级！"
			],
			"BAG_EMPTY": [
				"{0}，背包里没有食物啦~"
			],
			"PET_FULL": [
				"{0}，我已经吃得很饱了"
			]
		},
		"清洁": {
			SUCCESS_KEY: [
				"我爱洗澡，身体好好！我更爱{0}，跟我洗澡。哈哈！",
				"洗完澡后全身香香的，小伙伴们肯定更喜欢我了！",
				"我家的沐浴露真好用，洗完后全身清清爽爽。",
				"左搓搓，右搓搓，使劲搓，把每个毛孔都清洗干净，吼吼~"
			],
			"PET_DISABLE": [
				"{0}，不要为难我好不好~😔!"
			],
			"LEVEL_UP": [
				"{0}，我又升级啦！是不是变得更可爱了呢？",
				"真棒，我又升级了，都是{0}无微不至的照顾我才长得这么快的呢~",
				"{0}，我又升了一级！"
			],
			"BAG_EMPTY": [
				"{0}，背包里没有清洁用品啦~"
			],
			"PET_VERY_CLEAN": [
				"{0}，我已经洗得非常干净了，不用再继续洗了",
			]
		},
		"玩耍": {
			SUCCESS_KEY: [
				"终于又能出去玩喽，{0}，我好开心啊~😆"
			],
			"PET_VERY_DIRTY": [
				"{0}，我身上太脏了，先给我洗洗吧🥺",
			],
			"PET_VERY_HUNGRY": [
				"{0}，我好饿，先给我吃点东西吧🥺"
			],
			"PET_DISABLE": [
				"{0}，不要为难我好不好~😔!"
			],
			"LEVEL_UP": [
				"{0}，我又升级啦！是不是变得更可爱了呢？",
				"真棒，我又升级了，都是{0}无微不至的照顾我才长得这么快的呢~",
				"{0}，我又升了一级！"
			],
		}
	}
	
	static func query_pet_reply(order: String, key: String):
		key = key.to_upper()
		var index = randi_range(0, len(PET_DATA[order][key]) - 1)
		var result = PET_DATA[order][key][index]
		return result
		
	static func query_assistant_reply(order, key):
		key = key.to_upper()
		var index = randi_range(0, len(ASSISTANT_DATA[order][key]) - 1)
		var result = ASSISTANT_DATA[order][key][index]
		return result
	
	static func query_pet_reply_with_params(order: String, key: String, params):
		key = key.to_upper()
		var index = randi_range(0, len(PET_DATA[order][key]) - 1)
		var result: String = PET_DATA[order][key][index]
		return result.format(params)
	
	static func query_assistant_reply_with_params(order, key, params):
		key = key.to_upper()
		var index = randi_range(0, len(ASSISTANT_DATA[order][key]) - 1)
		var result: String = ASSISTANT_DATA[order][key][index]
		return result.format(params)
		
class PetConstant:
	const MAX_HUNGRY = 3000
	const MAX_CLEAN = 3000
	const MAX_HAPPY = 3000
	const MAX = 3000
	
	const TYPE_SET = {
		0: {
			"name": "t1",
			"max": 3000
		},
		1: {
			"name": "t2",
			"max": 3000
		},
		2: {
			"name": "t3",
			"max": 3000
		}
	}

	# 性别
	const SEX_MAPPING = {
		true: "公",
		false: "母",
		"公":  true,
		"母": false
	}
	# 成长阶段
	const STAGE_MAPPING = {
		"待孵化": 0,
		"宝宝": 1,
		"幼年": 2,
		"成年": 3,
		"已死亡": 4,
		0: "待孵化",
		1: "宝宝",
		2: "幼年",
		3: "成年",
		4: "已死亡",
	}
	# 默认用户数据
	const DEFAULT_USER_DATA : Dictionary= {
			
			"owner_id": null,
			"pet": {
				"call": "主人",
				"name": null,
				"sex": null,
				"level": 0,
				"stage": 0,
				"exp": 0,
				"hungry": 3000,
				"happy": 3000,
				"clean": 3000,
				"health": 3000,
				"money": 200
			},
			"bag": {
				"hungry": {
					1: 1,
					2: 9,
					3: 2,
					4: 1
				},
				"clean": {
					1: 2,
					2: 7,
					3: 2,
				}
			},
			"signin":{
				"keep": 0,
				"last_signin": null
			}
		}

class ActionQuerier:
	const ACTION_PATH_TEMPLATE: String = "QQPet/action/{0}/{1}/{2}"
	const ACTION_DATA = {
		"公":[
			{
				"孵化": ["First.gif"]
			},
			{
				"喂食": ["Eat2.gif"],
				"清洁": ["Clean.gif"],
				"玩耍": ["p1.gif", "p2.gif", "p3.gif","p4.gif","p5.gif", "p6.gif", "p7.gif", "p8.gif", "p9.gif", "p10.gif"],
			},
		],
		
		"母":[
			{
				"孵化": ["First.gif"]
			},
			{
				"喂食": ["Eat2.gif"],
				"清洁": ["Clean.gif"],
				"玩耍": ["p1.gif", "p2.gif", "p3.gif", "p4.gif","p5.gif", "p6.gif", "p7.gif", "p8.gif", "p9.gif", "p10.gif"]
			}
		]
	}

	# 查询动作路径
	static func query(sex, stage, action_name):
		Console.print_text(sex)
		Console.print_text(stage)
		Console.print_text(action_name)
		# 查询可用的动作表
		sex = PetConstant.SEX_MAPPING[sex]
		var action_list = ACTION_DATA[sex][stage][action_name]
		Console.print_text("可用的动作列表：")
		Console.print_text(action_list)
		if action_list == null or len(action_list) == 0:
			return null
		
		var index = randi_range(0, len(action_list) - 1)
		var action_file = action_list[index]
		if action_file == null:
			return null
			
		stage = PetConstant.STAGE_MAPPING[stage]
		var action_path =  ACTION_PATH_TEMPLATE.format([sex, stage, action_file])
		Console.print_text(action_path)
		return action_path

class GoodsQuerier:
	const GOODS_PATH_TEMPLATE = "QQPet/goods/{0}/{1}"

	const GOODS_DATA = {
		"hungry": {
			0: {
				"id": 0,
				"name": "甜蜜爱心",
				"value": 3000,
				"filename": "100010100.gif"
			},
			1: {
				"id": 1,
				"name": "冠军蛋糕",
				"value": 1440,
				"filename": "100010109.gif"
			},
			2: {
				"id": 2,
				"name": "月光饼饼",
				"value": 180,
				"filename": "102010003.gif"
			},
			3: {
				"id": 3,
				"name": "炸虾盖饭",
				"value": 1800,
				"filename": "102010005.gif"
			},
			4: {
				"id": 4,
				"name": "咯咯蛋蛋面",
				"value": 1440,
				"filename": "102010034.gif"
			}
		},
		"clean": {
			0: {
				"id": 0,
				"name": "强力电吹风",
				"value": 1080,
				"filename": "102020002.gif"
			},
			1: {
				"id": 1,
				"name": "秒喂香皂",
				"value": 1080,
				"filename:": "102020008.gif"
			},
			2: {
				"id": 2,
				"name": "魔幻矿泉泥",
				"value": 1080,
				"filename:": "102020020.gif"
			},
			3: {
				"id": 3,
				"name": "Q宠宝贝霜",
				"value": 360,
				"filename": "102020032.gif"
			}
		}
	}

	# 查询物品数据
	static func query(type, id):
		var goods = GOODS_DATA[type][id]
		return goods

	# 查询物品名称
	static func query_name(type, id):
		var goods = GOODS_DATA[type][id]
		if goods != null:
			return goods["name"]
		return null

	# 查询物品效果
	static func query_value(type, id):
		var goods = GOODS_DATA[type][id]
		if goods != null:
			return goods["value"]
		return null
